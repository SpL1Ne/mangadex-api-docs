daemon off;

pid /tmp/nginx.pid;
error_log stderr warn;
worker_processes auto;
worker_rlimit_nofile 65535;

events {
    multi_accept on;
    worker_connections 65535;
}

http {
    charset                utf-8;
    sendfile               on;
    tcp_nopush             on;
    tcp_nodelay            on;
    server_tokens          off;
    log_not_found          off;
    types_hash_max_size    2048;
    types_hash_bucket_size 64;
    client_max_body_size   16M;

    client_body_buffer_size 256k;
    client_body_in_file_only off;

    client_body_temp_path "/tmp/nginx-tmp/client_body" 1 2;
    proxy_temp_path       "/tmp/nginx-tmp/proxy_temp" 1 2;
    fastcgi_temp_path     "/tmp/nginx-tmp/fastcgi_temp" 1 2;
    scgi_temp_path        "/tmp/nginx-tmp/scgi_temp" 1 2;
    uwsgi_temp_path       "/tmp/nginx-tmp/uwsgi_temp" 1 2;

    log_format main '$time_iso8601 - $http_x_request_id - $remote_addr - "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"';
    access_log /dev/stdout main;

    include      /etc/nginx/mime.types;
    default_type application/octet-stream;

    # always do relative redirects, since nginx doesn't properly keep the proxied url scheme
    absolute_redirect off;
    # don't expose nginx internal listening port; it will break the redirect and leak information
    port_in_redirect off;

    map $uri $cache_control {
        # short cache for
        # - index.html
        # - api.yaml
        # - custom.css
        # - custom.js
        ~\/(index\.html|custom\.css|custom\.js|api\.ya?ml)?$ "public, max-age=10, s-max-age=30, stale-while-revalidate";
        default "public, max-age=84600, s-max-age=60, stale-while-revalidate";
    }

    # mangadex.org
    server {
        listen *:8080;
        root /var/www/mangadex;

        # sanitized at our edge
        set_real_ip_from 0.0.0.0/0;
        real_ip_header "X-Forwarded-For";
        real_ip_recursive on;

        # cors/security headers
        add_header Access-Control-Allow-Origin "*" always;
        add_header Expect-CT                   "enforce, max-age: 604800" always;
        add_header Permissions-Policy          "fullscreen=(self)" always;
        add_header Referrer-Policy             "strict-origin" always;
        add_header Strict-Transport-Security   "max-age=63072000; includeSubDomains; preload" always;
        add_header Via                         "$server_protocol mangadex-api-docs" always;
        add_header X-Frame-Options             "DENY" always;
        add_header X-XSS-Protection            "1; mode=block" always;
        add_header X-Content-Type-Options      "nosniff" always;

        # Do not 'always' this one, so it doesn't cause caching of 4xx and 5xx responses,
        # therefore poisoning our edge caches forever (in case of a race during a rolling update, for example)
        add_header Cache-Control               "$cache_control";

        # dot files
        location ~ /\.(?!well-known) {
            deny all;
        }

        # Default to denying paths not explicitly whitelisted
        location / {
            deny all;
        }

        # SPA-mode index.html
        location /docs {
            try_files $uri $uri/ $uri/index.html =404;
        }

        # gzip
        gzip            on;
        gzip_vary       on;
        gzip_proxied    any;
        gzip_comp_level 6;
        gzip_types      text/plain text/css text/xml application/json application/javascript application/rss+xml application/atom+xml image/svg+xml;
    }

}
